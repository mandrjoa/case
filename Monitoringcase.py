import os
import time
import git
from prometheus_client import start_http_server, Gauge

# Define Prometheus metrics
commits_diff_metric = Gauge('project_commits_diff', 'Number of commits difference')
contributors_metric = Gauge('project_contributors', 'Number of contributors')
repository_size_metric = Gauge('project_repository_size', 'Size of the repository')

# Path to Inkscape repo
repository_path = '/Users/yugo/Desktop/inkscape'
repo = git.Repo(repository_path)  # Open the Git repo once

def get_commits_and_contributors():
    # Fetch the latest changes from the remote repository
    origin = repo.remotes.origin
    origin.fetch()

    # Get the number of commits ahead and behind the remote
    ahead = list(repo.iter_commits(f'origin/{repo.active_branch.name}..HEAD'))
    behind = list(repo.iter_commits(f'HEAD..origin/{repo.active_branch.name}'))
    
    # Get the list of unique contributors
    contributors = set(commit.author.email for commit in repo.iter_commits())
    
    return len(ahead), len(behind), len(contributors)

def get_repository_size():
    size = 0
    for dirpath, dirnames, filenames in os.walk(repository_path):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            if os.path.exists(file_path):
                size += os.path.getsize(file_path)
    return size

if __name__ == '__main__':
    # Start Prometheus HTTP server on port 8080
    start_http_server(8080)
    print("Prometheus server started on port 8080")

    while True:
        ahead, behind, contributors = get_commits_and_contributors()

        # Update Prometheus metrics
        commits_diff_metric.set(ahead + behind)
        contributors_metric.set(contributors)
        size_bytes = get_repository_size()  # Retrieve repository size
        repository_size_metric.set(size_bytes)  # Update Prometheus metric

        print(f'Commits Difference: Ahead={ahead}, Behind={behind}')
        print(f'Number of Contributors: {contributors}')
        print(f'Repository Size: {size_bytes} bytes')  # Corrected variable name
        
        # Sleep for 5 minutes
        time.sleep(300)
        